public class TestRectangle {

    public static void main(String[] args) {
        Rectangle r = new Rectangle(5,6, new Point( 3, 7));

        System.out.println(" area =  " + r.area());

        Point[] corners = r.corners();
        for (int i =0; i<corners.length; i++){

            System.out.println("x = " + corners[i].xCoord + "y = " + corners[i].yCoord);
        }

        r.topLeft.xCoord =5;

        Rectangle r2 = new Rectangle(7,9, new Point( 3, 7));
        System.out.println(" area =  " + r2.area());
    }



}
